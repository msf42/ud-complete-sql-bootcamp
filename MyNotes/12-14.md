# Sections 12 - 14

## 12: Databases and Tables

### 73: Overview

Done

---

### 74: Creating and Restoring a Database

Super easy. Already did this before.

---

### 75: Challenge: Restoring a Database

Easy in the GUI

---

### 76: Restoring a Table Schema

Done

---

### 77: Challenge: Restore a Table Schema

Done

---

### 78: Review of Section

Done

---

## 13: Command Line Install Lectures

Not needed

---

## 14: BONUS SECTION: Thank You!

### 88: Bonus Lecture

Done.
