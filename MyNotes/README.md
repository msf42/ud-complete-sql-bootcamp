# Complete SQL Bootcamp

- [Sections 01 and 02: Intro, Fundamentals](01-02.md)

- [Sections 03 and 04: GROUP BY Statements, Assessment 1](03-04.md)

- [Sections 05 - 07: JOINS, Adv SQL Commands, Assessment 2](05-07.md)

- [Section 08: Creating Databases and Tables](08.md)

- [Sections 09 - 11: Assessment 3, Extra Views, PostGreSQL with Python](09-11.md)

- [Sections 12 - 14: Databases and Tables, Command Line, and Bonus](12-14.md)

---
