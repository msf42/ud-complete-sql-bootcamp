# Sections 01 and 02

## 02: SQL Statement Fundamentals

### 08: Overview of Challenges

![image-20191119094441561](images/Lesson08.png)

---

### 09: SQL Cheat Sheet

![image-20191119094547949](images/Lesson09.png)

---

### 10: SQL Statement Fundamentals

Syntax of this section applies to all SQL databases

---

### 11: SELECT Statement

```sql
SELECT column1, column 2 FROM table_name
SELECT * FROM actor;
SELECT first_name, last_name FROM actor;
```

---

### 12: Challenge: SELECT task

```sql
-- Get a list of all customer names
SELECT first_name, last_name, email FROM customer;
```

---

### 13: SELECT DISTINCT

```sql
SELECT DISTINCT column1, column2 FROM tableName;
```

---

### 14: Challenge: SELECT DISTINCT

```sql
-- Find list of ratings we have
SELECT DISTINCT rating FROM film;
```

---

### 15: SELECT WHERE

```sql
SELECT column1, column2
FROM tableName
WHERE conditions;
```

---

### 16: Challenge: SELECT WHERE

```sql
--1 Find email for customer Nancy Thomas
SELECT email
FROM customer
WHERE first_name = 'Nancy' AND last_name='Thomas';

--2 Find description for movie "Outlaw Hanky"
SELECT description
FROM film
WHERE title = 'Outlaw Hanky';

--3 Find the phone number for the person who lives at '259 Ipoh Drive'
SELECT phone
FROM address
WHERE address  = '259 Ipoh Drive';
```

---

### 17: COUNT

```sql
SELECT COUNT(*) FROM table;
SELECT COUNT(column) FROM table;
SELECT COUNT(DISTINCT firstName) FROM table;
```

---

### 18: LIMIT

```sql
SELECT * FROM table
LIMIT 5;
```

---

### 19: ORDER BY

```sql
SELECT column1, column2
FROM tableName
ORDER BY column1 DESC;
```

---

### 20: Challenge: ORDER BY

```sql
--1 Find the customer ID of those with the 10 hightest payment amounts
SELECT customer_ID
FROM payment
ORDER BY amount DESC
LIMIT 10;

--2 Get titles of movies with film ids 1-5
SELECT title
FROM film
ORDER BY film_id ASC
LIMIT 5;
```

---

### 21: BETWEEN

```sql
column1 BETWEEN 3 AND 5;
column2 NOT BETWEEN 5 AND 10;
```

---

### 22: IN

```sql
WHERE column1 IN (value1, value2, value3);

-- or in a subquery
WHERE column1 IN (SELECT column2 FROM tableName);
```

---

### 23: LIKE

```sql
-- Can't remember customer's name.... was it Jenny? Jen?
SELECT first_name, last_name
FROM customer
WHERE first_name LIKE 'Jen%';

-- ILIKE is case insensitive

-- % matches any sequence of characters
-- _ matches any single character
```

---

### 24: General Challenge 1

```sql
-- How many payment transactions were greater than $5?
SELECT COUNT(amount)
FROM payment
WHERE amount > 5;
-- 3618


-- How many actors have a first name that starts with 'P'?
SELECT COUNT(first_name)
FROM actor
WHERE first_name LIKE 'P%';
-- 5

-- How many unique districts are our customers from?
SELECT COUNT(DISTINCT(district))
FROM address;
-- 378

-- Retrieve a list of names for those distinct districts from the previous question.
SELECT DISTINCT(district)
FROM address;

-- How many films have a rating of R and a replacement cost between $5 and $15?
SELECT COUNT(*)
FROM film
WHERE rating = 'R' AND replacement_cost BETWEEN 5 AND 15;
-- 52

-- How many films have the word Truman somewhere in the title?
SELECT COUNT(*)
FROM film
WHERE title ILIKE '%truman%'
-- 5
```

---
