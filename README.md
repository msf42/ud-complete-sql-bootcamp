# Course Notes from Complete SQL Bootcamp

My course notes from Udemy [Complete SQL Bootcamp](https://www.udemy.com/course/the-complete-sql-bootcamp/)

The previous courses I took were MS-SQL based courses. This one uses PostgreSQL. I really enjoyed this one. The instructor, [Jose Portilla](https://www.udemy.com/user/joseportilla/), also taught the Python Bootcamp course I took. He is an excellent instructor and I highly recommend his courses.

![certificate](PostgreSQL.jpg)

---
